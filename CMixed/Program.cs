﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

//混合编程,C#调用C++的函数库
namespace CMixed
{
    class Program
    {
        public class NativeDLLCpp
        {
            [DllImport("NativeDLLCpp.dll")]
            public static extern int Test();
        }
        static void Main(string[] args)
        {
            #region CPP
            //int t = 10;
            //long total = 0;
            //do
            //{
            //    DateTime start = System.DateTime.Now;
            //    CppFunction cppFunction = new CppFunction();
            //    int result = 0;
            //    for (int i = 0; i < 10000; i++)
            //    {
            //        for (int j = 0; j < 10000; j++)
            //        {
            //            result += cppFunction.TestFunc(i, j);
            //        }
            //    }
            //    DateTime end = System.DateTime.Now;

            //    System.Console.WriteLine("Result: " + result + " Elapsed: " + (end - start).Milliseconds);

            //    t--;
            //    total += (end - start).Milliseconds;
            //} while (t > 0);
            //Console.WriteLine("共计：" + total + "\r\n输入任意键退出程序.");

            #endregion
            #region DllImport
            Console.WriteLine("DllImport方式进行处理.");
            int t = 10;
            long total = 0;
            do
            {
                Console.Write($"{t}.");
                DateTime start = System.DateTime.Now;
                int result = NativeDLLCpp.Test();
                DateTime end = System.DateTime.Now;
                t--;
                total += (end - start).Milliseconds;
            } while (t > 0);
            Console.WriteLine("共计：" + total + "\r\n输入任意键退出程序.");
            #endregion
            //Console.ReadKey();
        }
    }
}
