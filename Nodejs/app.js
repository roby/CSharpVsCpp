//'use strict';

function calc(m_a, m_b) {
    if (m_a % 2 == 0) {
        return m_a + m_b;
    }
    if (m_b % 2 == 0) {
        return m_a - m_b;
    }
    return m_b - m_a;
}

function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var seperator2 = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + date.getHours() + seperator2 + date.getMinutes()
        + seperator2 + date.getSeconds();
    return currentdate;
}


var intMax = 2147483647 + 1;
var t = 10;
var total = 0;
console.log(getNowFormatDate());
do
{
    var start = new Date().valueOf();
    var result = 0;
    for (var i = 0; i < 10000.0; i++) {
        for (var j = 0; j < 10000.0; j++) {
            result += calc(i, j);
        }
    }
    result = result % intMax;
    //450-->10
    //499500000 -->1000
    //499500000
    //499950000000-->10000
    //1733793664 -->10000  //right:
    //console.log('Result:' + result);

    var end = new Date().valueOf();

    console.log(t + ".Result: " + result + " Elapsed: " + (end - start));
    t--;
    total +=(end - start);
} while (t > 0);
console.log("Total is:"+ total);
console.log('Run Successfully!');

