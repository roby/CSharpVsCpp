﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpDotNetCore
{
    class CCalc
    {
        public CCalc(int a, int b)
        {
            m_a = a;
            m_b = b;
        }

        public int Calc()
        {
            if (m_a % 2 == 0)
            {
                return m_a + m_b;
            }
            if (m_b % 2 == 0)
            {
                return m_a - m_b;
            }
            return m_b - m_a;
        }

        private int m_a;
        private int m_b;
    }

    class CppFunction
    {
        public int TestFunc(int a, int b)
        {
            CCalc calc = new CCalc(a, b);
            return calc.Calc();
        }
    }
}
