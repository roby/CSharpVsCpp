﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            //int im = 0;// int.MinValue;
            //List<Object> lsim = new List<object>();
            //do
            //{
            //    Object o = new object();
            //    o = im;
            //    lsim.Add(o);
            //    im++;
            //} while (im<int.MaxValue);

            int t = 10;
            long total = 0;
            do
            {

                DateTime start = System.DateTime.Now;
                CppFunction cppFunction = new CppFunction();
                int result = 0;
                for (int i = 0; i < 10000; i++)
                {
                    for (int j = 0; j < 10000; j++)
                    {
                        result += cppFunction.TestFunc(i, j);
                    }
                }
                DateTime end = System.DateTime.Now;

                System.Console.WriteLine(t+".Result: " + result + " Elapsed: " + (end-start).Milliseconds);
                t--;
                total += (end - start).Milliseconds;
            } while (t > 0);
            Console.WriteLine("共计："+total+"\r\n输入任意键退出程序.");
            Console.ReadKey();
        }
    }
}
